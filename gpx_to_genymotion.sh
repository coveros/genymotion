#!/bin/bash

gps="gps.tmp"
#clean up our file
if [ -f $gps ]; then
  rm $gps
fi

#loop through each of our values in our gpx file
while IFS='' read -r line || [[ -n "$line" ]]; do
  #if this is a line with latitude and longitude information on it
  if [[ $line == \<trkpt* ]]; then
    #extract each 'interesting' value from our trkpt element
    lat=$(echo "$line" | sed -r 's/[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)T([0-9\.-\:]+).*/\1/g')
    lon=$(echo "$line" | sed -r 's/[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)T([0-9\.-\:]+).*/\2/g')
    ele=$(echo "$line" | sed -r 's/[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)T([0-9\.-\:]+).*/\3/g')
    day=$(echo "$line" | sed -r 's/[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)T([0-9\.-\:]+).*/\4/g')
    tim=$(echo "$line" | sed -r 's/[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)[^0-9\.-]*([0-9\.-]+)T([0-9\.-\:]+).*/\5/g')
    gps_time=`date -d "${day}T${tim}" +%s`

    #set our values into one file that can be read into genymotion
    echo "gps setlatitude $lat" >> $gps
    echo "gps setlongitude $lon" >> $gps
    echo "gps setaltitude $ele" >> $gps

    #calculate our heading, but only do that if we have previous values
    if [ ! -z "$old_lat" ]; then
      pi=`echo "4*a(1)" | bc -l`
      #figure out the different of our longitude
      diff_lon=`echo $lon - $old_lon | bc`
      #calculate our y value for our heading
      diff_long_rads=`echo "$diff_lon*($pi/180)" | bc -l`
      lat_rads=`echo "$lat*($pi/180)" | bc -l`
      y=`echo "s($diff_long_rads)*c($lat_rads)" | bc -l`
      #calculate our x value for our heading
      old_lat_rads=`echo "$old_lat*($pi/180)" | bc -l`
      x=`echo "c($old_lat_rads)*s($lat_rads) - s($old_lat_rads)*c($lat_rads)*c($diff_long_rads)" | bc -l`
      #calculate our heading from our x and y values
      if (( $(echo "$x == 0" | bc -l) )); then
        heading=$pi
      else
        heading=`echo "a($y/$x)" | bc -l`
      fi
      heading=`echo "($heading*180)/$pi" | bc -l`
      #if our heading is below 0, add 360
      if (( $(echo "$heading < 0" | bc -l) )); then
        heading=`echo $heading + 360 | bc`
      fi
      #while our heading is above 360, subtract 360
      while (( $(echo "$heading > 360" | bc -l) )); do
        heading=`echo $heading - 360 | bc`
      done
      echo "gps setbearing $heading" >> $gps

      #determine how long to wait before our next input
      time_diff=$((gps_time - old_gps_time))
      echo "pause $time_diff" >> $gps
    fi

    #save off our old values
    old_lat=$lat
    old_lon=$lon
    old_ele=$ele
    old_day=$day
    old_tim=$tim
    old_gps_time=$gps_time
  fi
done < "$1"

/opt/genymotion/genymotion-shell -f $gps
rm $gps
