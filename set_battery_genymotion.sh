#!/bin/bash

if [ "$#" -ne "3" ]; then
  echo "This script requires 3 parameters."
  echo "  Initial Battery Percentage (between 0 and 100)"
  echo "  Battery Status (charging or discharging)"
  echo "  Time for completion (in minutes)"
  exit 1
fi
battery=$1
charge=$2
time=$3

#how often should we update
pause=1

file="battery.tmp"
if [ -f $file ]; then
  rm $file
fi
echo "battery setmode manual" >> $file
echo "battery setstatus $charge" >> $file
echo "battery setlevel $battery" >> $file
echo "pause $pause" >> $file

get_to=100
if [ "$charge" == "discharging" ]; then
  get_to=0
fi
diff=$((get_to-battery))
updates=`echo $diff / $time / 60 | bc -l`
updates="$(echo "$updates*$pause" | bc)"

curtime=`date +%s`
while [ true ]; do
  battery=`echo $battery+$updates | bc -l`
  update_to=`echo "scale=0;($battery+0.5)/1" | bc`
  echo "battery setlevel $update_to" >> $file
  if [ $update_to -eq $get_to ]; then
    break
  fi
  echo "pause $pause" >> $file
done

/opt/genymotion/genymotion-shell -f $file
rm $file
