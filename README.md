Multiple scripts to automate some genymotion hooks into device and OS APIs


## gpx_to_genymotion
A shell script to extract the latitude, longitude, and elevation from a gpx file, and read it into Genymotion based on the datetimes provided in the file

 The basic workflow of the script is as below:
 * Read in our file, and for each line starting with *trkpt* process it
 * Extract out the latitude, longitude, elevation, and timestamp
 * Write the latitude, longitude, and elevation to a tmp file
 * If we have previous values, calculate the heading
 * Create a time differential from the timestamp to the current time
 * If enough time has passed, execute our update of gps command

NOTES
You may need to modify the location of where genymotion is installed
You need an emulator up and running already to execute this script call
This script takes one parameter, and the parameter is the gpx file
Depending on the structure of your gpx file, you might have to change how you parse in your latitude, longitude, elevation, and datetime stamps.

## set_battery_genymotion
A shell script to automatically update your battery power settings in real time. The script requests desired initial battery level, whether you want to charge or discharge, and how long you want this operation to take
